# Dracula for [NATRON](https://natrongithub.github.io)

> A dark theme for [NATRON](https://natrongithub.github.io).

![Screenshot](./screenshot.png)

## Install

All instructions can be found at [draculatheme.com/natron](./INSTALL.md).

## Team

This theme is maintained by the following person(s) and a bunch of [awesome contributors](https://github.com/dracula/natron/graphs/contributors).

| [![Lu15ange7](https://gitlab.com/uploads/-/system/user/avatar/10161541/avatar.png?size=25)](https://gitlab.com/lu15ange7)
|---------------------------------------------------------------------------------------------|
| [Lu15ange7](https://gitlab.com/lu15ange7)                                               | 

## Community

- [Twitter](https://twitter.com/draculatheme) - Best for getting updates about themes and new stuff.
- [GitHub](https://github.com/dracula/dracula-theme/discussions) - Best for asking questions and discussing issues.
- [Discord](https://draculatheme.com/discord-invite) - Best for hanging out with the community.

## License

[MIT License](./LICENSE)
