### [NATRON](https://natrongithub.github.io)

#### Install using Git

If you are a git user, you can install the theme and keep up to date by cloning the repo:

```bash
git clone https://gitlab.com/Lu15ange7/dracula-natron.git
```

#### Install manually

Download using the [GitLab `.zip` download](https://gitlab.com/Lu15ange7/dracula-natron/-/archive/main/dracula-natron-main.zip) option and unzip them.

#### Save theme file

Open Natron and select

`Edit > Preferences`

![Screenshot](./conf_0.png)

Now select

`> Appearence`

StyleSheet file (.qqs) open the `dracula.qss` file and click on `Save` button.

![Screenshot](./conf_1.png)

#### Theme Fix

If exist some color without change just fix it with this way:

`Appearence > Main Window`

In `Surken`, `Base` and `Raised` just change the color to `#787989`

In `Selection` and `Timeline playhead` just change the color to `#bd93f9`

![Screenshot](./conf_2.png)


`Appearence > Dope Sheet`

In `Sheet background color` change the color to `#787989`

![Screenshot](./conf_3.png)

